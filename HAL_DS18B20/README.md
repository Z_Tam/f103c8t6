## 驱动支持
|驱动|说明|
|:--|:--|
|DS18B20|温度传感器|

---
## 参考文档
### DS18b20
[STM32F103 DS18B20](https://blog.csdn.net/weixin_40774605/article/details/88557470?ops_request_misc=&request_id=&biz_id=102&utm_term=f103c8t6%2018b20&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-88557470.pc_search_result_hbase_insert&spm=1018.2226.3001.4187)