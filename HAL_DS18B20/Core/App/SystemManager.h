#ifndef __SYSTEMMANAGER_H
#define __SYSTEMMANAGER_H

#define PowerEn GPIO_PB14
#define LED GPIO_PB2

void SystemManagerInit(void);
void SystemManagerRun(void);

#endif
