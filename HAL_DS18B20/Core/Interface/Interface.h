#ifndef __INTERFACE_H
#define __INTERFACE_H

//<<< Use Configuration Wizard in Context Menu >>>
//<o> Chip Type  <0=>Unknow  <1=>f103c8
#define CHIP_TYPE 1
//<<< end of configuration section >>>

#include "GPIO.h"
#include "DELAY.h"

/*
**记录设备状态
*/
typedef enum
{
    DRIVE_UNDEFINE = 0u,
    DRIVE_EXIST,
    DRIVE_READY,
    DRIVE_RUNNING,
    DRIVE_STOP,
} DRIVE_STATE;



/*
**DS18B20数据
*/
typedef struct
{
    DRIVE_STATE state;
    double temp;
}_DS18B20;
extern _DS18B20 _ds18b20;

/*
**记录设备数据 
*/
typedef struct
{
    _DS18B20 _ds18b20;
}DRIVE;
extern DRIVE drive;

#endif
