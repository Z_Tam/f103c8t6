/*******************************
**  f103c8_gpio中间层，
**  提供标准IO读写，配置
*******************************/
#include "GPIO.h"
#include "Interface.h"

//#if (CHIP_TYPE == 1)
static GPIO_TypeDef *GPIOx[] = {GPIOA, GPIOB, GPIOC, GPIOD, GPIOE};
static uint16_t GPIOx_Pin[] = {GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_3, GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_8, GPIO_PIN_9 \
								, GPIO_PIN_10, GPIO_PIN_11, GPIO_PIN_12, GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15, GPIO_PIN_All};


/*****************************************************************************
**函数名称:	 	GPIO_WritePin
**函数功能:	 	设置IO口电平高低
**入口参数:
**返回参数:
******************************************************************************/
void GPIO_WritePin(LPGPIO_PinType pin, GPIO_PinState PinState)
{
	if(pin < GPIO_END)
	{
		HAL_GPIO_WritePin((GPIO_TypeDef *)GPIOx[pin / 16], GPIOx_Pin[pin % 16], (GPIO_PinState)PinState);
	}
}

GPIO_PinState GPIO_ReadPin(LPGPIO_PinType pin)
{
   return HAL_GPIO_ReadPin((GPIO_TypeDef *)GPIOx[pin / 16], GPIOx_Pin[pin % 16]);
}

/*****************************************************************************
**函数名称:	 	GPIO_Setdir
**函数功能:	 	修改IO输入输出方向
**入口参数:
**返回参数:
******************************************************************************/
void GPIO_Setdir(LPGPIO_PinType pin, LPGPIO_Dir dir)
{
  if(pin < GPIO_END){
     if(pin % 16 > 7)                                             //CRH
     {
        switch(dir)
        {
          case Intput:
            GPIOx[pin / 16]->CRH &= ~(0xF<< ((pin%16 - 7)*4) );   //先清空对应寄存器
            GPIOx[pin / 16]->CRH |= (8<<  ((pin%16 - 7)*4)  );    //设置为输入
          break;
          case Output:
            GPIOx[pin / 16]->CRH &= ~(0xF<< ((pin%16 - 7)*4) ); 
            GPIOx[pin / 16]->CRH |= (3<<  ((pin%16 - 7)*4)  );    
          break;
          default:
          break;  
          
        }
     }
     else{                                                        //CRL
       switch(dir)
        {
          case Intput:
            GPIOx[pin / 16]->CRL &= ~(0xF<< ((pin%16)*4) ); 
            GPIOx[pin / 16]->CRL |= (8<<  ((pin%16)*4)  );    
          break;
          case Output:
            GPIOx[pin / 16]->CRL &= ~(0xF<< ((pin%16)*4) ); 
            GPIOx[pin / 16]->CRL |= (3<<  ((pin%16)*4)  );    
          break;
          default:
          break;  
          
        }
     }
  }
}

//#endif






