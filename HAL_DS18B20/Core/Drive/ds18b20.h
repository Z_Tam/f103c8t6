#ifndef __DS18B20_H
#define __DS18B20_H

#include "Interface.h"

#define DS18B20 GPIO_PB8


void ds18b20_reset(void);
void ds18b20_run(void);
#endif
