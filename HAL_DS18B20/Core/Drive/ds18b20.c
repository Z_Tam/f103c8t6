#include "ds18b20.h"
#include "stdio.h"

_DS18B20 _ds18b20;

/*
**读一个位
*/
uint8_t DS18B20_Rbit(void)
{
    //rbit是最终位数据，x是取状态变量
    uint8_t rbit = 0x00,x = 0;
    //改变DQ为输出模式
    GPIO_Setdir(DS18B20, Output);
    GPIO_WritePin(DS18B20,GPIO_PIN_SET);
    //总线写0
    GPIO_WritePin(DS18B20,GPIO_PIN_RESET);
    //延时15us以内
    delay_us(1);
    //释放总线
    GPIO_WritePin(DS18B20,GPIO_PIN_SET);
    //改变DQ为输入模式
    GPIO_Setdir(DS18B20, Intput);
    //延时大约3us
    //delay_us(7);
    //获取总线电平状态
    x = GPIO_ReadPin(DS18B20);
    //如果是1，则返回0x80，否则返回0x00
    if(x)
        rbit = 0x80;
    //延时大约60us
    delay_us(130);
    return rbit;
}


/*
**读一个字节
*/
unsigned char ds18b20_Rbyte(void)
{
	uint8_t rbyte = 0,i = 0, tempbit =0;
    for (i = 1; i <= 8; i++)
    {    
        tempbit = DS18B20_Rbit();   //读取位
        rbyte = rbyte >> 1;         //右移实现高低位排序
        rbyte = rbyte|tempbit;      //或运算移入数据
    }
    return rbyte;
}

/*
**写一个字节
*/
void ds18b20_Wbyte(unsigned char dat)
{
	unsigned char i=0;
    GPIO_Setdir(DS18B20, Output);  
	
	for(i=8;i>0;i--)
	{
        GPIO_WritePin(DS18B20,GPIO_PIN_SET);    //先拉高释放总线
		GPIO_WritePin(DS18B20,GPIO_PIN_RESET);
        delay_us(25);
		if(dat&0x01)
            GPIO_WritePin(DS18B20,GPIO_PIN_SET);
		delay_us(25);                           //60-120us
		GPIO_WritePin(DS18B20,GPIO_PIN_SET);    //释放总线
        delay_us(4);
		dat>>=1;
	}
}

/*
**复位并检测ds18b20是否存在
*/
void ds18b20_reset(void)
{
    int ack = -1;
    GPIO_WritePin(DS18B20,GPIO_PIN_SET);    //先拉高
    delay_us(10);
    GPIO_WritePin(DS18B20,GPIO_PIN_RESET);  //拉低复位脉冲
    delay_us(600);                          //480-960
    GPIO_WritePin(DS18B20,GPIO_PIN_SET);    //拉高等待回复
    delay_us(30);                           //15-60
    GPIO_Setdir(DS18B20, Intput);           //修改IO为输入
    delay_us(10);   
    ack = GPIO_ReadPin(DS18B20);            //读取输入
    delay_us(300);                          //等待释放数据总线
    GPIO_Setdir(DS18B20, Output);
    
    if(drive._ds18b20.state == DRIVE_UNDEFINE && ack == 0)
    {
        drive._ds18b20.state = DRIVE_EXIST;
    }
}

//计算温度值
//参数 高字节，低字节
double CaculateTemp(uint8_t tmh, uint8_t tml)
{
    uint8_t th;
	uint8_t tl;
	double temp = 0;
 
	tl = tml & 0x0F;//取低字节后四位
 
	th = (tmh << 4) + (tml >> 4);//取高字节后三位和低字节前四位
	
	temp = (int)th;//整数部分
 
	if (tmh > 0x08)
	{
		th = ~th + 1;//取反加一
		temp = -th;//负数
	}
 
	temp += tl * 0.0625;//小数部分
 
	//printf(" Ds18b20ReadData temp=%3.3f \n", temp);
	return temp;
}

/*
**获取一次温湿度数据
*/
void ds18b20_get_once(void)
{
    //DS18B20初始化
    ds18b20_reset();
    //跳过读序列号
    ds18b20_Wbyte(0xcc);
    //启动温度转换
    ds18b20_Wbyte(0x44);
    //等待温度转换
    HAL_Delay(1);
    ds18b20_reset();
    ds18b20_Wbyte(0xcc);
    //读温度寄存器
    ds18b20_Wbyte(0xbe); 
    uint8_t TempL = ds18b20_Rbyte();
    uint8_t TempH = ds18b20_Rbyte();
    //符号位为负
    if(TempH > 0x70)
    {
        TempL = ~TempL;
        TempH = ~TempH;
    }
    //整数部分
    drive._ds18b20.temp = CaculateTemp(TempH,TempL);
}

/*
**判断运行状态并执行
*/
void ds18b20_run(void)
{
    switch(drive._ds18b20.state)
    {
        case DRIVE_EXIST:
            printf("ds18b20 exist\r\n");
            drive._ds18b20.state = DRIVE_RUNNING;
            ds18b20_get_once();
        break;
        case DRIVE_RUNNING:
            ds18b20_get_once();
            printf("18b20: %3.3f\r\n",drive._ds18b20.temp);
        break;
        default:
        break;
    }

}




