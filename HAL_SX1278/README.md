# 模块

<div align=left>
<img src="_img/1.jpg" width = 20%/>
</div>

[官网链接](https://www.silicontra.com/product_Info-44-160.html#lij)


# 实物
<div align=left>
<img src="_img/2.jpg" width = 50%/>
</div>

# 接线
| MCU pin | SX1278 pin | Description |
| ------- | ---------- | ----------- |
| PA4     | NSS        | SPI chip--select |
| PA5     | SCK        | SPI SCK     |
| PA6     | SO         | SPI MISO    |
| PA7     | SI         | SPI MOSI    |
| PB0     | DIO0       | LoRa interrupt indicator pin (for receiver mode) |
| PB1     | RST        | LoRa reset  |
| ------- | ---------- | ----------- |
| VDD     | VCC        | +3V3        |
| VSS     | GND        | GND         |

# 结果
<div align=left>
<img src="_img/3.jpg" width = 50%/>
</div>


