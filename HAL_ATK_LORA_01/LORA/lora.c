#include "lora.h"
#include "FIFO.h"
#include "string.h"

void lora_check(void);
static uint8_t lora_send_cmd(uint8_t cmd_index, char *arg);
static void lora_config(LORA_CFG cfg);
static void lora_showcfg(void);
static void lora_transmit(uint8_t *buf);
static uint16_t lora_reserve(uint8_t *buf);
static uint16_t serial_reserve(uint8_t *buf);
static void serial_transmit(uint8_t *buf);

/********************************
	功能：初始化配置参数
*********************************/
LORA_CFG lora_cfg = 
{
	0x0000,
	0x23,
	0x05,
	0x03,
	0x00,
	0x00,
	0x00,
	0x07,
	0x00,
};

/************************************
	功能：使用结构体指针，指向各个函数
*************************************/
LORA_FUNC LORA_func = 
{
	.check = lora_check,
	.config = lora_config,
	.showcfg = lora_showcfg,
	.transmit = lora_transmit,
	.reserve = lora_reserve,
	.ser_tx = serial_transmit,
	.ser_rx = serial_reserve,
};

/*********************************
	功能：检测模块是否存在，是否正忙
	提示：若检测不到请先检查接线
*********************************/
static void lora_check(void)
{
	
	uint8_t buf[100] = {0}, buf_i = 0;
	MDO_H		/* 拉高进入配置模式 */
	while(1)
	{
		if(0 == AUX_R)
		{
			memset(buf,0,sizeof(buf));
			buf_i = 0;
			HAL_UART_Transmit(&huart3, (uint8_t *)lora_cmd_list[0], strlen(lora_cmd_list[0]), 0xFFFF);
			HAL_Delay(100);
			while(pop_one_fifo(&uart3_fifo,&buf[buf_i++]));
			
			if(strstr((char *)buf,"OK") != NULL)
			{
				break;
			}
			else
			{
				printf("lora no found\r\n");
			}
		}else
		{
			printf("lora busy ...\r\n");
			HAL_Delay(100);
		}
	}
	MDO_L
}

/****************************
	功能：用于发送配置指令
****************************/
static uint8_t lora_send_cmd(uint8_t cmd_index, char *arg)
{
	char buf[20] = {0}, i = 0;

	strcat(buf,lora_cmd_list[cmd_index]);	/* 拼接控制命令 */
	if(arg != NULL){
		strcat(buf,arg);					/* 参数字符串 */
	}
	strcat(buf,"\r\n");						/* 拼接回车换行 */
	while(AUX_R);							/* 等待模块不忙 */
	HAL_UART_Transmit(&huart3, (uint8_t *)buf, strlen(buf), 0xFFFF);
	HAL_Delay(10);
	memset(buf, 0, sizeof(buf));
	while(pop_one_fifo(&uart3_fifo,(uint8_t *)&buf[i++]));
	if(NULL != strstr(buf,"OK")){
		if(strstr(arg,"?")){
			HAL_UART_Transmit(&huart1, (uint8_t *)buf+1, strlen(buf)-5, 0xFFFF);
		}
		return 1;
	}
	else{
		printf("error: %s\r\n",buf);
		HAL_Delay(100);
		return 0;
	}
}

/*************************
	功能：lora模式配置
**************************/
static void lora_config(LORA_CFG cfg)
{ 
	char buf[10] = {0};		/* 参数缓存 */
	
	printf("##### lora_config #####\r\n");
	MDO_H
	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%.2x,%.2x",cfg.ADDR>>8,cfg.ADDR&0xFF);	/* 地址 */
	while(0==lora_send_cmd(1, buf));
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%x,%x\r\n",cfg.RATE,cfg.CNN);				/* 速率，信道 */
	while(0==lora_send_cmd(2, buf));
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%x\r\n",cfg.TPOWER);						/* 发射功率 */
	while(0==lora_send_cmd(3, buf));
	
	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%x\r\n",cfg.CWMODE);						/* 工作模式 */
	while(0==lora_send_cmd(4, buf));

	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%x\r\n",cfg.TMODE);						/* 传输模式 */
	while(0==lora_send_cmd(5, buf));

	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%x\r\n",cfg.WLTIME);						/* 睡眠时间 */
	while(0==lora_send_cmd(6, buf));

	memset(buf,0,sizeof(buf));
	sprintf(buf,"=%x,%x\r\n",cfg.BAUD, cfg.DCHECK);			/* 波特率，校验位 */
	while(0==lora_send_cmd(7, buf));
	MDO_L
}

/**************************
	功能：查询配置
***************************/
static void lora_showcfg(void)	
{
	printf("##### lora_showcfg #####\r\n");			

	MDO_H
	while(0==lora_send_cmd(9, NULL));						/* 关闭回显 */
	while(0==lora_send_cmd(1, "?"));
	while(0==lora_send_cmd(2, "?"));
	while(0==lora_send_cmd(3, "?"));
	while(0==lora_send_cmd(4, "?"));
	while(0==lora_send_cmd(5, "?"));
	while(0==lora_send_cmd(6, "?"));
	while(0==lora_send_cmd(7, "?"));
	MDO_L
}

/**************************** 
	功能：数据发送
****************************/
static void lora_transmit(uint8_t *buf)
{
	MDO_L
	while(AUX_R);
	//printf("TX num: %d, buf: %s",strlen((char *)buf),buf);
	HAL_UART_Transmit(&huart3, buf, strlen((char *)buf), 0xFFFF);
}

/****************************
	功能：lora数据接收
	输入：数组名
	返回：数据个数
*****************************/
static uint16_t lora_reserve(uint8_t *buf)
{
	volatile uint16_t i = 0;
	
	/*
	** 若有数据，堵塞至读完所有数据
	*/
	while(pop_one_fifo(&uart3_fifo,(uint8_t *)&buf[i]))
		i++;
	if(strlen((char *)buf)>0)
	{
		return i;
	}else
	{
		return 0;
	}
}


static uint16_t serial_reserve(uint8_t *buf)
{
	volatile uint16_t i = 0;
	
	while(pop_one_fifo(&uart1_fifo,(uint8_t *)&buf[i]))
		i++;
	if(strlen((char *)buf)>0)
	{
		return i;
	}else
	{
		return 0;
	}
}

static void serial_transmit(uint8_t *buf)
{
	printf("%s",buf);
}




