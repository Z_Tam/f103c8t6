#ifndef __LORA_H
#define __LORA_H

#include "stdio.h"
#include "usart.h"
#include "stm32f1xx_hal.h"

/***************************
	功能：LORA控制指令
****************************/
static const char lora_cmd_list[20][10] = 
{
	"AT\r\n",			/* 0：检测模块是否存在 */
	"AT+ADDR",			/* 1：地址 */
	"AT+WLRATE",		/* 2：信道和速率 */
	"AT+TPOWER",	  	/* 3：发射功率 */
	"AT+CWMODE",		/* 4：工作模式 */
	"AT+TMODE",			/* 5：传输模式 */
	"AT+WLTIME",		/* 6：睡眠时间 */
	"AT+UART",			/* 7：波特率和数据校验位 */
	"ATE1",				/* 8: 打开回显 */
	"ATE0",				/* 9：关闭回显 */
};

/*****************************
	功能：LORA配置参数
******************************/
typedef struct
{
	uint16_t ADDR;		/* 地址 */
	uint8_t RATE;		/* 速率 */
	uint8_t CNN;		/* 信道 */
	uint8_t TPOWER;		/* 发射功率 */
	uint8_t CWMODE;		/* 工作模式 */
	uint8_t TMODE;		/* 传输模式 */
	uint8_t WLTIME;		/* 睡眠时间 */
	uint8_t BAUD;		/* 波特率 */
	uint8_t DCHECK;		/* 数据校验位 */
}LORA_CFG;	
extern LORA_CFG lora_cfg;

/***************************************
	功能：定义结构体指针函数的参数格式
****************************************/
typedef struct 
{
	void (*check)(void);
	void (*config)(LORA_CFG);
	void (*showcfg)(void);
	void (*transmit)(uint8_t *);
	uint16_t (*reserve)(uint8_t *);
	uint16_t (*ser_rx)(uint8_t *);
	void (*ser_tx)(uint8_t *);
}LORA_FUNC;
extern LORA_FUNC LORA_func;

/*****************************
	功能：GPIO设置
******************************/
#define MDO_H \
{\
	if(0==(GPIOA->ODR & 0x4))\
	{\
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_SET);\
		HAL_Delay(50);\
	}\
}
#define MDO_L \
{\
	if(GPIOA->ODR & 0x4)\
	{\
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);\
		HAL_Delay(50);\
	}\
}
#define AUX_R HAL_GPIO_ReadPin( GPIOA, GPIO_PIN_3)

#endif
