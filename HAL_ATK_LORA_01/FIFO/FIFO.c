#include "FIFO.h"
#include "stdio.h"
#include "usart.h"


extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart3_rx;

/**************************
	串口相关数据
***************************/
#define Uart1BufLen 100
#define Uart3BufLen 100
static uint8_t Uart1RxBuf[Uart1BufLen] = {0};
static uint8_t Uart3RxBuf[Uart3BufLen] = {0};

/**************************
	定义fifo相关数据
***************************/
FIFO_TYPE uart1_fifo = {0};
FIFO_TYPE uart3_fifo = {0};

/***************************
	单个数据压入缓存
****************************/
uint8_t push_one_fifo(FIFO_TYPE* fifo , uint8_t data)
{
	if(fifo->len < FIFO_MAX_LEN){
		fifo->buff[fifo->head]=data;		
		fifo->head=(fifo->head+1) % FIFO_MAX_LEN; 
		fifo->len++;
		return 1;
	}
	return 0;
}
 
/***************************
	缓存数据弹出
****************************/
uint8_t pop_one_fifo(FIFO_TYPE* fifo,uint8_t* data)
{
	if(fifo->len > 0)						
	{
		*data = fifo->buff[fifo->tail];    /*取出一个字节数据*/
		/*头指针加1，到达最大值清零，构建环形队列*/
		fifo->tail=(fifo->tail+1) % FIFO_MAX_LEN;  
		fifo->len--;
		return 1;
	}		
	return 0;
}

/******************************
	多个数据压入
*******************************/
uint8_t push_fifo(FIFO_TYPE* fifo,uint8_t* data,uint16_t len)
{
	if((fifo->len + len) < FIFO_MAX_LEN)
	{
		for(int i=0;i<len;i++)
		{
			push_one_fifo(fifo , data[i]);
		}
		return 1;
	}
	return 0;
}

/*******************************
	多个数据弹出
********************************/
void pop_fifo(FIFO_TYPE* fifo,uint8_t* data, uint16_t len)
{
	for(int i=0;i<len;i++)
	{
		pop_one_fifo(fifo,data);
	}
}

/***********************************
	串口1中断回调函数
************************************/
void UART1_IRQ(void)
{
  if(huart1.Instance==USART1)
  {
	if(RESET != __HAL_UART_GET_FLAG(&huart1, UART_FLAG_IDLE))   	//判断是否是空闲中斿
    {
        HAL_UART_DMAStop(&huart1);									//停止DMA
		push_fifo(&uart1_fifo, Uart1RxBuf, Uart1BufLen - __HAL_DMA_GET_COUNTER(&hdma_usart1_rx));
		HAL_UART_Receive_DMA(&huart1, Uart1RxBuf, Uart1BufLen);		//重新接收			
    }
	__HAL_UART_CLEAR_IDLEFLAG(&huart1);                     	//清除空闲中断标志（否则会丿直不断进入中断）
  }
}

/***********************************
	串口3中断回调函数
************************************/
void UART3_IRQ(void)
{
  if(huart3.Instance==USART3)
  {
	if(RESET != __HAL_UART_GET_FLAG(&huart3, UART_FLAG_IDLE))   	//判断是否是空闲中斿
    {
        HAL_UART_DMAStop(&huart3);									//停止DMA
		push_fifo(&uart3_fifo, Uart3RxBuf, Uart3BufLen - __HAL_DMA_GET_COUNTER(&hdma_usart3_rx));
		HAL_UART_Receive_DMA(&huart3, Uart3RxBuf, Uart3BufLen);		//重新接收			
    }
	__HAL_UART_CLEAR_IDLEFLAG(&huart3);                     	//清除空闲中断标志（否则会丿直不断进入中断）
  }
}

