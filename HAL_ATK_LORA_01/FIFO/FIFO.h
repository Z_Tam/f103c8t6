#ifndef __FIFO_H
#define __FIFO_H

#include "stm32f1xx_hal.h"

/**************************
	FIFO相关参数
***************************/
#define FIFO_MAX_LEN 1024
typedef struct
{
	uint16_t head;              	//队列头
	uint16_t tail;                //队列尾
	uint16_t len;                	//当前队列数据长度
	char buff[FIFO_MAX_LEN];  	//队列数组
}FIFO_TYPE;

/****************************
	两个串口的句柄
*****************************/
extern FIFO_TYPE uart1_fifo;
extern FIFO_TYPE uart3_fifo;

/***************************
	声明
****************************/
uint8_t pop_one_fifo(FIFO_TYPE* fifo,uint8_t* data);
uint8_t push_fifo(FIFO_TYPE* fifo,uint8_t* data,uint16_t len);
void pop_fifo(FIFO_TYPE* fifo,uint8_t* data,uint16_t len);

#endif
