#ifndef __TMF8801_H
#define __TMF8801_H
//#include "fsl_common.h"
#include "usart.h"
#include "main.h"
/**
 * \name TMF8x01 寄存器地址定义 
 * @{
 */
 
#define TMF8x01_REGADDR_COMMAND         0x10
#define TMF8x01_REGADDR_STATUS          0x1D       
#define TMF8x01_REGADDR_CONTENTS        0x1E
#define TMF8x01_REGADDR_TID             0x1F
#define TMF8x01_REGADDR_ENABLE          0xE0

/** @} */

/**
 * \name TMF8x01 函数错误返回值定义
 * @{
 */
 
#define TMF8x01_OK                      0         /**< \brief 操作成功        */
#define TMF8x01_NK                     -1         /**< \brief 操作失败        */

#define TMF8x01_ENORES                 -2         /**< \brief 结果未生成      */
#define TMF8x01_ENOTREADY              -4         /**< \brief 传感器忙        */

/** @} */

#define NO_DATA_PROVIDE                 0         /**< 不提供数据 */
#define FACTORY_CALIBRATION_PROVIDE     1         /**< 提供了工厂校准数据 */
#define ALGORITHM_STATE_PROVIDE         2         /**< 提供了算法校准数据 */

#define GPIO0_INPUT                     0         /**< GPIO0 用作输入 */
/** GPIO0 为低时禁能测量，为高时启动测量 */
#define GPIO0_LOW_STOP_HIGH_START       1
/** GPIO0 为高时禁能测量，为低时启动测量 */
#define GPIO0_HIGH_STOP_LOW_START       2
#define GPIO0_OUTPUT                    3         /**< GPIO0 用作输出 */
#define GPIO0_OUTPUT_LOW                4         /**< GPIO0 输出低 */
#define GPIO0_OUTPUT_HIGH               5         /**< GPIO0 输出高 */

#define GPIO1_INPUT                    (0 << 4)   /**< GPIO1 用作输入 */
/** GPIO1 为低时禁能测量，为高时启动测量 */
#define GPIO1_LOW_STOP_HIGH_START      (1 << 4)
/** GPIO1 为高时禁能测量，为低时启动测量 */
#define GPIO1_HIGH_STOP_LOW_START      (2 << 4)
#define GPIO1_OUTPUT                   (3 << 4)   /**< GPIO1 用作输出 */
#define GPIO1_OUTPUT_LOW               (4 << 4)   /**< GPIO1 输出低 */
#define GPIO1_OUTPUT_HIGH              (5 << 4)   /**< GPIO1 输出高 */

/** \brief APP0 配置初始化结构体定义 */
struct app0_init_config
{
    /* 工厂校准和算法状态掩码 */
    uint8_t calibration_state_mask;   
    
    /* GPIOx 控制 */
    uint8_t gpio_control;
  
    /* GPIOx 输出控制 */
    uint8_t gpio_output_control;
    
    /* 连续测量周期设置 */
    uint8_t repetition_period_ms;
    
    /* 迭代次数 */
    uint16_t iterations;
};
typedef struct app0_init_config      app0_init_config_t;  

struct result_info
{
    uint8_t  result_num;             /* 0x20,      结果 ID */
    uint8_t  reliability;            /* 0x21[5:0], 置信度 */
    uint16_t distance_peak;          /* 0x22~0x23, 距离(mm) */
    uint32_t time_stamp;             /* 0x25~0x28, 时间戳(0.2us) */
    uint32_t reference_hits;         /* 0x33~0x36, 距离模式下，参考 SPADs 被光子击中总数量 */
    uint32_t object_hits;            /* 0x37~0x40, 距离模式下，目标 SPADs 被光子击中总数量 */
};
typedef struct result_info      result_info_t;

extern result_info_t   g_tof_result;    /**< \brief 存储 TOF 获取的结果 */

/**
 * \brief TMF8801 初始化
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 */
int8_t tmf8801_init (void);

/**
 * \brief TMF8801 APP0 配置并启动
 *
 * \param[in]  p_config   : APP0 配置参数
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 */
int8_t tmf8801_app0_start (app0_init_config_t *p_config);

/**
 * \brief TMF8801 获取时间戳
 *
 * \param[in]  timestamp : 时间戳(0.2us)
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 * \retval TMF8x01_ENORES : 结果未生成
 */
int8_t tof_timestamp_get (uint8_t *timestamp);

/**
 * \brief TMF8801 获取结果
 *
 * \param[out]  p_res   : 存放获取的结果
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 * \retval TMF8x01_ENORES    : 结果未生成
 */
int8_t tmf8801_result_get (result_info_t *p_res);

/**
 * \brief 显示 TMF8801 获取结果
 */
void tmf8801_result_display (result_info_t result);

/**
 * \brief TMF8801 停止 APP0
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 */
int8_t tmf8801_app0_stop (void);

/**
 * \brief TMF8801 工厂校准
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 * \retval TMF8x01_ENOTREADY : 传感器忙
 */
int8_t tmf8801_factory_calibration (void);

/**
 * \brief TMF8801 获取 APP0 配置默认值
 *
 * \param[out]  p_config   : APP0 配置参数
 */
void tmf8801_app0_get_default_config (app0_init_config_t *p_config);

/**
 * \brief 调整 TMF8x01 晶振频率
 *
 * \param[in]  change    晶振频率(MHz)    VCSEL发射频率(MHz)
 *            -----------------------------------------------
 *              -20         5.0                39.0
 *              -50         4.5                36.0
 *              -100        3.9                31.2
 *              -200        3.1                24.8
 *
 * \retval TMF8x01_OK : 操作成功
 * \retval TMF8x01_NK : 操作失败
 */
int8_t tmf8801_app0_osc_trim (int32_t change);

void tmf8801_intx_enable (void);
uint8_t tmf8801_int_status_get (void);
void tmf8801_int_status_clr (void);
int8_t tmf8801_handle_irq (void);
    
#endif
