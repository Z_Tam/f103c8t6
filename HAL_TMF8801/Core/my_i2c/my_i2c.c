#include "my_i2c.h"

void iic_delay_us(uint8_t time)
{
	uint8_t i;
  while(time--)
	
	for(i=0;i<6;i++);
}

/*********************************************************
 * 名称: TMF8001_I2C_Start
 * 功能: 模拟I2C的起始条件
 * 形参: 无        
 * 返回: 无
 * 说明: 标准的I2C协议I2C起始条件：SCL为高时，SDA由高变低             
 *********************************************************/
void TMF8001_I2C_Start(void)
{
    TMF8001_I2C_SDA_Out;
    TMF8001_I2C_SCL_HIGHT;
    TMF8001_I2C_SDA_HIGHT;
    iic_delay_us(4);
    TMF8001_I2C_SDA_LOW;
    //标准的I2C协议I2C起始条件：SCL为高时，SDA由高变低
    iic_delay_us(4);
    TMF8001_I2C_SCL_LOW;//钳住I2C总线，准备发送或接收数据 
}

/*********************************************************
 * 名称: TMF8001_I2C_Stop
 * 功能: 模拟I2C的停止条件
 * 形参: 无        
 * 返回: 无
 * 说明: 标准的I2C协议I2C停止条件：SCL为高时，SDA由低变高             
 *********************************************************/
void TMF8001_I2C_Stop(void)
{
    TMF8001_I2C_SDA_Out;
    TMF8001_I2C_SCL_LOW;
    TMF8001_I2C_SDA_LOW;
    iic_delay_us(4);
    TMF8001_I2C_SCL_HIGHT;
    TMF8001_I2C_SDA_HIGHT;
    iic_delay_us(4);
}

/*********************************************************
 * 名称: TMF8001_I2C_Wait_Ack
 * 功能: 等待应答信号
 * 形参: 无        
 * 返回: 0：接收成功 1：接收失败
 * 说明: 无       
 *********************************************************/
uint8_t TMF8001_I2C_Wait_Ack(void)
{
	uint8_t ucErrTime=0;
	TMF8001_I2C_SDA_In;
	TMF8001_I2C_SDA_HIGHT;
	iic_delay_us(4);	   
	TMF8001_I2C_SCL_HIGHT;
	iic_delay_us(4);	 
	while(Get_TMF8001_I2C_SDA)
	{
		ucErrTime ++;
		if(ucErrTime > 250)
		{
			TMF8001_I2C_Stop();
			return 1;
		}
	}
	TMF8001_I2C_SCL_LOW;
	return 0;
}

/*********************************************************
 * 名称: TMF8001_I2C_Ack
 * 功能: 模拟I2C的应答
 * 形参: 无        
 * 返回: 无
 * 说明: 根据标准的I2C协议，从I2C从器件读1个byte后
         也就是在第9个CLK的时候，I2C主设备的SDA要变低
         表示I2C主器件已经接收完一个字节byte，I2C从器件
         继续发送数据              
 *********************************************************/
void TMF8001_I2C_Ack(void)
{
    TMF8001_I2C_SCL_LOW;
    TMF8001_I2C_SDA_Out;
    TMF8001_I2C_SDA_LOW;
    iic_delay_us(4);
    TMF8001_I2C_SCL_HIGHT;
    iic_delay_us(4);
    TMF8001_I2C_SCL_LOW;
}

/*********************************************************
 * 名称: TMF8001_I2C_NAck
 * 功能: 模拟I2C的不应答
 * 形参: 无        
 * 返回: 无
 * 说明:            
 *********************************************************/
void TMF8001_I2C_NAck(void)
{
    TMF8001_I2C_SCL_LOW;
    TMF8001_I2C_SDA_Out;
    TMF8001_I2C_SDA_HIGHT;
    iic_delay_us(4);
    TMF8001_I2C_SCL_HIGHT;
    iic_delay_us(4);
    TMF8001_I2C_SCL_LOW;
}

/*********************************************************
 * 名称: TMF8001_I2C_Send_Byte
 * 功能: 模拟I2C发送一个字节
 * 形参: Byte        
 * 返回: 无
 * 说明:              
 *********************************************************/
void TMF8001_I2C_Send_Byte(uint8_t Byte)
{
    uint8_t bit;
    TMF8001_I2C_SDA_Out;
    TMF8001_I2C_SCL_LOW;
    for(bit=0;bit<8;bit++)
    {
        if((Byte&0x80)>>7)
        {
            TMF8001_I2C_SDA_HIGHT;
        }
        else 
        {
            TMF8001_I2C_SDA_LOW;
        }
        Byte<<=1;
        //iic_delay_us(4);
        TMF8001_I2C_SCL_HIGHT;
        iic_delay_us(4);
        TMF8001_I2C_SCL_LOW;
        iic_delay_us(4);
    }
}

/************************************************
 * 名称: TMF8001_I2C_Read_Byte
 * 功能: 模拟I2C读取一个字节
 * 形参: 无       
 * 返回: 1个字节
 * 说明: 无              
 **************************************************/
uint8_t TMF8001_I2C_Read_Byte(uint8_t ack)
{
    uint8_t bit,Receive_Byte=0;
    TMF8001_I2C_SDA_In;
    for(bit=0;bit<8;bit++)
    {
        TMF8001_I2C_SCL_LOW;
        iic_delay_us(4);
        TMF8001_I2C_SCL_HIGHT;
        Receive_Byte<<=1;//把上一时刻的值左移一位
        if(Get_TMF8001_I2C_SDA)
        {
            Receive_Byte|=0x01;
        }
        iic_delay_us(4);
    }
    if(!ack)
    {
        TMF8001_I2C_NAck();
    }
    else
    {
        TMF8001_I2C_Ack();
    }
    return Receive_Byte;
}

/*******************************************************************************
 * 名称: I2C_TMF8001_WriteByte
 * 功能: 通过I2C协议向TMF8001写入1个字节
 * 形参: Data_com -> 要写入的数据
 * 返回: 无
 * 说明: 无 
 ******************************************************************************/
void I2C_TMF8001_WriteByte(uint8_t Addr, uint8_t *Data_Com, uint8_t Len)
{
    uint8_t i;
    TMF8001_I2C_Start();	/*I2C Start*/
    TMF8001_I2C_Send_Byte(TMF8001_Address);	/*Send TMF8001 Driver  address*/
    TMF8001_I2C_Ack();
    TMF8001_I2C_Send_Byte(Addr);	/*Send byte*/
    TMF8001_I2C_Ack();
    for(i=0;i<Len;i++)
    {
        TMF8001_I2C_Send_Byte(Data_Com[i]);	/*Send byte*/
        TMF8001_I2C_Ack();
    }
    TMF8001_I2C_Stop();	/*I2C Stop*/
}

/*******************************************************************************
 * 名称: I2C_TMF8001_ReadByte
 * 功能: 通过I2C协议从TMF8001读取6个字节
 * 形参: Addr -> 写入寄存器地址 *Read_Buf -> 存数据的数组地址
 * 返回: 1个字节数据
 * 说明: 无 
 ******************************************************************************/
uint8_t I2C_TMF8001_ReadByte(uint8_t Addr, uint8_t *Read_Buf, uint8_t Len)
{	
  uint8_t i;
	TMF8001_I2C_Start();/*I2C start*/
	TMF8001_I2C_Send_Byte(TMF8001_Address);	/*Send TMF8001 Driver  address*/
  TMF8001_I2C_Ack();
	TMF8001_I2C_Send_Byte(Addr);	/*Send byte*/
  TMF8001_I2C_Ack();
    
	TMF8001_I2C_Start();/*I2C start*/
	TMF8001_I2C_Send_Byte(TMF8001_Address|0x01);/*Send ReadCommand*/
	TMF8001_I2C_Ack();
	for(i=0;i<Len;i++)
	{
	    if(i == (Len - 1))
	    {
	        Read_Buf[i] = TMF8001_I2C_Read_Byte(0);/*I2C ReadByte*/
	    }
	    else
	    {
            Read_Buf[i] = TMF8001_I2C_Read_Byte(1);/*I2C ReadByte*/
	    }
	}
	TMF8001_I2C_Stop();/*I2C Stop*/
//	printf("Read_Buf0: %02X ",Read_Buf[0]);
}
















