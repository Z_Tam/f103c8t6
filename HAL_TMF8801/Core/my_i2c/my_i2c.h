#ifndef __MY_I2C_H
#define __MY_I2C_H

#include "main.h"
#include <stdint.h>

#define TMF8001_I2C_PORT             GPIOB
#define TMF8001_I2C_SCL_PIN          GPIO_PIN_6
#define TMF8001_I2C_SDA_PIN          GPIO_PIN_7

#define TMF8001_I2C_SCL_HIGHT    		 HAL_GPIO_WritePin(TMF8001_I2C_PORT, TMF8001_I2C_SCL_PIN, GPIO_PIN_SET)
#define TMF8001_I2C_SCL_LOW      		 HAL_GPIO_WritePin(TMF8001_I2C_PORT, TMF8001_I2C_SCL_PIN, GPIO_PIN_RESET)
#define TMF8001_I2C_SDA_HIGHT    		 HAL_GPIO_WritePin(TMF8001_I2C_PORT, TMF8001_I2C_SDA_PIN, GPIO_PIN_SET)
#define TMF8001_I2C_SDA_LOW     		 HAL_GPIO_WritePin(TMF8001_I2C_PORT, TMF8001_I2C_SDA_PIN, GPIO_PIN_RESET)

#define TMF8001_I2C_SDA_Out      		 {TMF8001_I2C_PORT->CRL&=0X0FFFFFFF;TMF8001_I2C_PORT->CRL|=3<<(4*7);}		/* PB7 */
#define TMF8001_I2C_SDA_In       	   {TMF8001_I2C_PORT->CRL&=0X0FFFFFFF;TMF8001_I2C_PORT->CRL|=8<<(4*7);}		/* PB7 */
#define Get_TMF8001_I2C_SDA          HAL_GPIO_ReadPin(TMF8001_I2C_PORT, TMF8001_I2C_SDA_PIN)

#define TMF8001_Address              0x82

void iic_delay_us(uint8_t time);
void TMF8001_I2C_Init(void);
void TMF8001_I2C_Start(void);
void TMF8001_I2C_Stop(void);
void TMF8001_I2C_Ack(void);
void TMF8001_I2C_NAck(void);
void TMF8001_I2C_Send_Byte(uint8_t Byte);
uint8_t TMF8001_I2C_Read_Byte(uint8_t ack);

uint8_t I2C_TMF8001_ReadByte(uint8_t Addr, uint8_t *Read_Buf, uint8_t Len);
void I2C_TMF8001_WriteByte(uint8_t Addr, uint8_t *Data_Com, uint8_t Len);
int crc8_compute(uint8_t *check_data,uint8_t num_of_data);
int shtc3_crc8_check(uint8_t *p,uint8_t num_of_data,uint8_t CrcData);
void TMF8001_Config(void);
float GetTemp_Humi_value(void);


#endif
